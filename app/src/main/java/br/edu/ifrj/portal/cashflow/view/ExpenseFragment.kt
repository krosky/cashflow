package br.edu.ifrj.portal.cashflow.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import br.edu.ifrj.portal.cashflow.databinding.FragmentExpenseBinding
import br.edu.ifrj.portal.cashflow.viewmodel.ExpenseViewModel

class ExpenseFragment : Fragment() {
    private var _binding: FragmentExpenseBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, s: Bundle?): View {
        val mViewModel =
            ViewModelProvider(this)[ExpenseViewModel::class.java]

        _binding = FragmentExpenseBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}