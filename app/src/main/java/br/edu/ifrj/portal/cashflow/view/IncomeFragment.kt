package br.edu.ifrj.portal.cashflow.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import br.edu.ifrj.portal.cashflow.databinding.FragmentIncomeBinding
import br.edu.ifrj.portal.cashflow.viewmodel.IncomeViewModel

class IncomeFragment : Fragment() {
    private var _binding: FragmentIncomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, s: Bundle?): View {
        val mViewModel =
            ViewModelProvider(this)[IncomeViewModel::class.java]

        _binding = FragmentIncomeBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}