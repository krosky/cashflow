package br.edu.ifrj.portal.cashflow.service.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transactions")
class TransactionModel {
    @PrimaryKey(autoGenerate = true) var id: Int = 0
    var description: String = ""
    var date: String = ""
    @ColumnInfo(name = "money_value") var moneyValue: String = ""
    @ColumnInfo(name = "transaction_type") var transactionType: Boolean = false
}