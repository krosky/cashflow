package br.edu.ifrj.portal.cashflow.service.repository

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import br.edu.ifrj.portal.cashflow.service.model.TransactionModel

interface TransactionDAO {
    @Insert
    fun insert(transaction: TransactionModel): Long

    @Query("SELECT * FROM transactions WHERE id = :id")
    fun get(id: Int): TransactionModel
    @Query("SELECT * FROM transactions")
    fun getTransactions(): List<TransactionModel>
    @Query("SELECT * FROM transactions WHERE transaction_type = 1")
    fun getIncomes(): List<TransactionModel>
    @Query("SELECT * FROM transactions WHERE transaction_type = 0")
    fun getExpenses(): List<TransactionModel>

    @Update
    fun update(transaction: TransactionModel): Int

    @Delete
    fun delete(transaction: TransactionModel)
}