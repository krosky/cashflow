package br.edu.ifrj.portal.cashflow.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.edu.ifrj.portal.cashflow.R

class TransactionFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_form)
    }
}