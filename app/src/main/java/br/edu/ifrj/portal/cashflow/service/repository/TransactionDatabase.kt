package br.edu.ifrj.portal.cashflow.service.repository

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase

abstract  class TransactionDatabase: RoomDatabase() {
    abstract fun transactionDAO(): TransactionDAO

    companion object {
        private lateinit var INSTANCE: TransactionDatabase
        fun getDatabase(context: Context): TransactionDatabase {
            if (!::INSTANCE.isInitialized) {
                synchronized(TransactionDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context, TransactionDatabase::class.java, "")
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }
    }

}